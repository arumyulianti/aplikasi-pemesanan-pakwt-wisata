/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package sisteminformasipaketwisata;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.awt.event.KeyEvent;
import java.sql.*;
import java.util.HashMap;
import javax.swing.JOptionPane;
import javax.swing.JSpinner;
import javax.swing.table.DefaultTableModel;
import koneksi.koneksi;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author ASUS
 */
public class pemesanan extends javax.swing.JFrame {
private Connection conn = new koneksi().connect();
private DefaultTableModel tabmode;

public String tempat_wisata;
public String idpelanggan,nama,noktp,jkel,alamat,notelp,email;
public String idpaketwisata,tujuan,tempatwisata,hargaperorang,makan,lamahari;
public String idbuswisata,notelpon,namabus,jenisbus,tarifbus;





Object[]Baris={"ID Pelanggan","ID Bus Wisata","ID Paket Wisata","Tujuan","Tempat Wisata","Harga Per Pack","Lama Hari","Jumlah Orang","Tambahan Biaya","Total Biaya"};

    
    /**
     * Creates new form pelanggan
     */
    public pemesanan() {
        initComponents();
        this.setLocationRelativeTo(null);
        new Thread(){
            public void run(){
                while(true){
                    Calendar kal = new GregorianCalendar();
                    int tahun = kal.get (Calendar.YEAR);
                    int bulan = kal.get (Calendar.MONTH)+1;
                    int hari = kal.get (Calendar.DAY_OF_MONTH);
                    int jam = kal.get (Calendar.HOUR_OF_DAY);
                    int menit = kal.get (Calendar.MINUTE);
                    int detik = kal.get (Calendar.SECOND);
                    String tanggal = tahun+"-"+bulan+"-"+hari;
                    String waktu1 = jam+":"+menit+":"+detik;
                    String waktu2 = jam+"-"+menit+"-"+detik;
                    jLabel3.setText(tanggal);
                    jLabel4.setText(waktu1);
                }
            }
        }.start();
        kosong();
        aktif();
        autonumber();
    }
    
    protected void autonumber(){
    try{
        String sql = "SELECT idnota FROM nota order by idnota asc";
        Statement stat = conn.createStatement();
        ResultSet result=stat.executeQuery(sql);
        txtidnota.setText("IN0001");
        while(result.next()){
        String idnota = result.getString("idnota").substring(2);
        int nomor = Integer.parseInt(idnota) + 1;
        String nol = "";
            if (nomor<10)
            {nol = "000";}
            else if (nomor<100)
            {nol = "00";}
            else if (nomor<1000)
            {nol = "";}
            
        txtidnota.setText("IN" + nol + nomor);
        }
    }
        catch (Exception e){
        JOptionPane.showMessageDialog(null,"Penomoran Salah" + e);
        }
    }
    
    public void itemTerpilih1(){
    popuppelanggan Pp = new popuppelanggan();
    Pp.pelanggan = this;
    txtidpelanggan.setText(idpelanggan);
    txtnama.setText(nama);
    txtalamat.setText(alamat);
    txtnotelp.setText(notelp);
    }
    
    public void itemTerpilih2(){
    popuparmada Pp = new popuparmada();
    Pp.armada = this;
    txtidbuswisata.setText(idbuswisata);
    txtnamabus.setText(namabus);
    cbjenisbus.setSelectedItem(jenisbus);
    txttarifbus.setText(tarifbus);
    }
    
    public void itemTerpilih3(){
    popuppaketwisata Pp = new popuppaketwisata();
    Pp.paketwisata3 = this;
    txtidpaketwisata.setText(idpaketwisata);
    cbtujuan.setSelectedItem(tujuan);
    txttempatwisata.setText(tempatwisata);
    txtharga.setText(hargaperorang);
    txtlamahari.setText(lamahari);
   
    }
    
     protected void aktif(){
        txtjumlahorang.requestFocus();
        jtgl.setEditor(new JSpinner.DateEditor(jtgl,"dd/MM/yyyy"));
        tabmode = new DefaultTableModel(null, Baris);
        tabeltransaksi.setModel(tabmode);
        txtidnota.setEnabled(false);
    }
     
    protected void kosong(){
        txtidpelanggan.setText("");
        txtnama.setText("");
        txtalamat.setText("");
        txtnotelp.setText("");
        txtidbuswisata.setText("");
        txtnamabus.setText("");
        cbjenisbus.setSelectedItem("");
        txttarifbus.setText("");
        txtidpaketwisata.setText("");
        cbtujuan.setSelectedItem("");
        txttempatwisata.setText("");
        txtharga.setText("");
        txtlamahari.setText("");
        txtjumlahorang.setText("");
        txttambahanbiaya.setText("");
        txttotalbiaya.setText("");
    }
    
    public void cetak(){
    try{
        String path="./src/report/nota4.jasper";
        HashMap parameter = new HashMap();
        parameter.put("idnota", txtidnota.getText());
        JasperPrint print = JasperFillManager.fillReport(path,parameter,conn);
        JasperViewer.viewReport(print,false);
    } catch (Exception ex){
        JOptionPane.showMessageDialog(rootPane,"Dokumen Tidak Ada"+ex);
    }   
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        jLabel22 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel21 = new javax.swing.JLabel();
        jSeparator3 = new javax.swing.JSeparator();
        jSeparator4 = new javax.swing.JSeparator();
        jPanel2 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        txtidpelanggan = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        txtnama = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtalamat = new javax.swing.JTextArea();
        jLabel13 = new javax.swing.JLabel();
        txtnotelp = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        txtidbuswisata = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        txtnamabus = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        cbjenisbus = new javax.swing.JComboBox();
        jLabel18 = new javax.swing.JLabel();
        txttarifbus = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        txtidpaketwisata = new javax.swing.JTextField();
        jLabel26 = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        cbtujuan = new javax.swing.JComboBox();
        jLabel28 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        txttempatwisata = new javax.swing.JTextArea();
        jLabel29 = new javax.swing.JLabel();
        txtharga = new javax.swing.JTextField();
        jLabel30 = new javax.swing.JLabel();
        txtjumlahorang = new javax.swing.JTextField();
        jLabel32 = new javax.swing.JLabel();
        txtlamahari = new javax.swing.JTextField();
        jLabel33 = new javax.swing.JLabel();
        jLabel34 = new javax.swing.JLabel();
        jLabel36 = new javax.swing.JLabel();
        txttotalbiaya = new javax.swing.JTextField();
        jLabel37 = new javax.swing.JLabel();
        txttambahanbiaya = new javax.swing.JTextField();
        tambah = new javax.swing.JButton();
        jPanel6 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tabeltransaksi = new javax.swing.JTable();
        txtidnota = new javax.swing.JTextField();
        jLabel19 = new javax.swing.JLabel();
        jtgl = new javax.swing.JSpinner();
        jLabel31 = new javax.swing.JLabel();
        simpan = new javax.swing.JButton();
        reset = new javax.swing.JButton();
        hapus = new javax.swing.JButton();
        jPanel11 = new javax.swing.JPanel();
        jLabel35 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);

        jPanel1.setBackground(new java.awt.Color(0, 51, 51));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/logo travel report.png"))); // NOI18N
        jLabel1.setMaximumSize(new java.awt.Dimension(110, 110));
        jLabel1.setMinimumSize(new java.awt.Dimension(110, 110));
        jLabel1.setPreferredSize(new java.awt.Dimension(50, 50));

        jLabel2.setFont(new java.awt.Font("News701 BT", 1, 12)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Jl. Penjalin No.2, RT 07/03, Desa Pasirsari, Cikarang Selatan, Bekasi, 17550");

        jLabel3.setBackground(new java.awt.Color(255, 255, 255));
        jLabel3.setFont(new java.awt.Font("Times New Roman", 2, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Tanggal");

        jLabel4.setBackground(new java.awt.Color(255, 255, 255));
        jLabel4.setFont(new java.awt.Font("Times New Roman", 2, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Waktu");

        jLabel5.setBackground(new java.awt.Color(255, 255, 255));
        jLabel5.setFont(new java.awt.Font("Times New Roman", 2, 14)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Tanggal :");

        jLabel6.setBackground(new java.awt.Color(255, 255, 255));
        jLabel6.setFont(new java.awt.Font("Times New Roman", 2, 14)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("Waktu :");

        jLabel24.setFont(new java.awt.Font("News701 BT", 1, 24)); // NOI18N
        jLabel24.setForeground(new java.awt.Color(255, 255, 255));
        jLabel24.setText("SISTEM INFORMASI PEMESANAN PAKET WISATA");

        jLabel17.setFont(new java.awt.Font("News701 BT", 1, 18)); // NOI18N
        jLabel17.setForeground(new java.awt.Color(255, 255, 255));
        jLabel17.setText("GOLDEN PRIMA TOUR AND TRAVEL");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(51, 51, 51)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(27, 27, 27)
                        .addComponent(jLabel24, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(145, 145, 145)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addGap(34, 34, 34)
                                .addComponent(jLabel17, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGap(192, 192, 192))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, 567, Short.MAX_VALUE)
                                .addGap(165, 165, 165)))))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel6)
                    .addComponent(jLabel5))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4)
                    .addComponent(jLabel3))
                .addGap(57, 57, 57))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(16, 16, 16)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(jLabel3)
                            .addComponent(jLabel24))
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(25, 25, 25)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel4)
                                    .addComponent(jLabel6)))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel17)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel2))))
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel5.setBackground(new java.awt.Color(0, 51, 51));

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 27, Short.MAX_VALUE)
        );

        jPanel7.setBackground(new java.awt.Color(0, 153, 153));

        jLabel22.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/Home-icon (1).png"))); // NOI18N
        jLabel22.setToolTipText("HOME");
        jLabel22.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel22MouseClicked(evt);
            }
        });

        jLabel23.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/close-icon (2).png"))); // NOI18N
        jLabel23.setToolTipText("EXIT");
        jLabel23.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel23MouseClicked(evt);
            }
        });

        jLabel21.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/Files-icon.png"))); // NOI18N
        jLabel21.setToolTipText("LAPORAN");
        jLabel21.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel21MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addComponent(jLabel23)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator3, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jSeparator1)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel7Layout.createSequentialGroup()
                                .addGap(9, 9, 9)
                                .addComponent(jLabel22))
                            .addGroup(jPanel7Layout.createSequentialGroup()
                                .addGap(8, 8, 8)
                                .addComponent(jLabel21)))
                        .addGap(0, 9, Short.MAX_VALUE))
                    .addComponent(jSeparator4, javax.swing.GroupLayout.Alignment.TRAILING))
                .addContainerGap())
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup()
                .addGap(74, 74, 74)
                .addComponent(jLabel22)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(64, 64, 64)
                .addComponent(jLabel21)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(64, 64, 64)
                .addComponent(jLabel23)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator4, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel2.setBackground(new java.awt.Color(0, 153, 153));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Data Pelanggan"));

        jLabel8.setFont(new java.awt.Font("GeoSlab703 Md BT", 1, 14)); // NOI18N
        jLabel8.setText("ID Pelanggan");

        txtidpelanggan.setFont(new java.awt.Font("Microsoft YaHei Light", 0, 12)); // NOI18N
        txtidpelanggan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtidpelangganActionPerformed(evt);
            }
        });

        jLabel9.setFont(new java.awt.Font("GeoSlab703 Md BT", 1, 14)); // NOI18N
        jLabel9.setText("Nama");

        txtnama.setFont(new java.awt.Font("Microsoft YaHei Light", 0, 12)); // NOI18N

        jLabel12.setFont(new java.awt.Font("GeoSlab703 Md BT", 1, 14)); // NOI18N
        jLabel12.setText("Alamat");

        txtalamat.setColumns(20);
        txtalamat.setFont(new java.awt.Font("Microsoft YaHei Light", 0, 12)); // NOI18N
        txtalamat.setRows(5);
        jScrollPane1.setViewportView(txtalamat);

        jLabel13.setFont(new java.awt.Font("GeoSlab703 Md BT", 1, 14)); // NOI18N
        jLabel13.setText("No Telfon");

        txtnotelp.setFont(new java.awt.Font("Microsoft YaHei Light", 0, 12)); // NOI18N

        jLabel7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/data-add-icon.png"))); // NOI18N
        jLabel7.addAncestorListener(new javax.swing.event.AncestorListener() {
            public void ancestorMoved(javax.swing.event.AncestorEvent evt) {
            }
            public void ancestorAdded(javax.swing.event.AncestorEvent evt) {
                jLabel7AncestorAdded(evt);
            }
            public void ancestorRemoved(javax.swing.event.AncestorEvent evt) {
            }
        });
        jLabel7.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel7MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel8)
                    .addComponent(jLabel9)
                    .addComponent(jLabel12)
                    .addComponent(jLabel13))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtnotelp, javax.swing.GroupLayout.DEFAULT_SIZE, 238, Short.MAX_VALUE)
                            .addComponent(txtnama)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 212, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(10, 10, 10))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(txtidpelanggan)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel7)
                        .addGap(4, 4, 4)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel8)
                        .addComponent(txtidpelanggan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel7))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel9)
                    .addComponent(txtnama, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(48, 48, 48)
                        .addComponent(jLabel12))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtnotelp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel13))
                .addContainerGap(15, Short.MAX_VALUE))
        );

        jPanel3.setBackground(new java.awt.Color(0, 153, 153));
        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Data Bus"));

        jLabel11.setFont(new java.awt.Font("GeoSlab703 Md BT", 1, 14)); // NOI18N
        jLabel11.setText("ID Bus");

        txtidbuswisata.setFont(new java.awt.Font("Microsoft YaHei Light", 0, 12)); // NOI18N
        txtidbuswisata.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtidbuswisataActionPerformed(evt);
            }
        });

        jLabel14.setFont(new java.awt.Font("GeoSlab703 Md BT", 1, 14)); // NOI18N
        jLabel14.setText("Nama Bus");

        txtnamabus.setFont(new java.awt.Font("Microsoft YaHei Light", 0, 12)); // NOI18N

        jLabel15.setFont(new java.awt.Font("GeoSlab703 Md BT", 1, 14)); // NOI18N
        jLabel15.setText("Jenis Bus");

        cbjenisbus.setFont(new java.awt.Font("Microsoft YaHei Light", 0, 12)); // NOI18N
        cbjenisbus.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Mikro Bus", "Medium Bus", "Big Bus" }));
        cbjenisbus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbjenisbusActionPerformed(evt);
            }
        });

        jLabel18.setFont(new java.awt.Font("GeoSlab703 Md BT", 1, 14)); // NOI18N
        jLabel18.setText("Tarif Bus");

        txttarifbus.setFont(new java.awt.Font("Microsoft YaHei Light", 0, 12)); // NOI18N
        txttarifbus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txttarifbusActionPerformed(evt);
            }
        });

        jLabel20.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/data-add-icon.png"))); // NOI18N
        jLabel20.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel20MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel14)
                    .addComponent(jLabel15)
                    .addComponent(jLabel18))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txttarifbus)
                    .addComponent(txtnamabus, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(cbjenisbus, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(txtidbuswisata)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel20)))
                .addContainerGap())
            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel3Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jLabel11)
                    .addContainerGap(292, Short.MAX_VALUE)))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(txtidbuswisata, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel20))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel14)
                    .addComponent(txtnamabus, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbjenisbus, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel15))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel18)
                    .addComponent(txttarifbus, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel11)
                    .addGap(211, 211, 211)))
        );

        jPanel4.setBackground(new java.awt.Color(0, 153, 153));
        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder("Data Paket Wisata"));

        txtidpaketwisata.setFont(new java.awt.Font("Microsoft YaHei Light", 0, 12)); // NOI18N

        jLabel26.setFont(new java.awt.Font("GeoSlab703 Md BT", 1, 14)); // NOI18N
        jLabel26.setText("ID Paket Wisata");

        jLabel27.setFont(new java.awt.Font("GeoSlab703 Md BT", 1, 14)); // NOI18N
        jLabel27.setText("Tujuan");

        cbtujuan.setFont(new java.awt.Font("Microsoft YaHei Light", 0, 12)); // NOI18N
        cbtujuan.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Bogor", "Yogyakarta", "Sukabumi", "Bandung", "Malang", "Baturaden", "Garut", "Pangandaran", "Dieng", "Majalengka" }));
        cbtujuan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbtujuanActionPerformed(evt);
            }
        });

        jLabel28.setFont(new java.awt.Font("GeoSlab703 Md BT", 1, 14)); // NOI18N
        jLabel28.setText("Tempat Wisata");

        txttempatwisata.setColumns(20);
        txttempatwisata.setFont(new java.awt.Font("Microsoft YaHei Light", 0, 12)); // NOI18N
        txttempatwisata.setRows(5);
        jScrollPane2.setViewportView(txttempatwisata);

        jLabel29.setFont(new java.awt.Font("GeoSlab703 Md BT", 1, 14)); // NOI18N
        jLabel29.setText("Harga Per Pack");

        txtharga.setFont(new java.awt.Font("Microsoft YaHei Light", 0, 12)); // NOI18N

        jLabel30.setFont(new java.awt.Font("GeoSlab703 Md BT", 1, 14)); // NOI18N
        jLabel30.setText("Jumlah Orang");

        txtjumlahorang.setFont(new java.awt.Font("Microsoft YaHei Light", 0, 12)); // NOI18N
        txtjumlahorang.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtjumlahorangActionPerformed(evt);
            }
        });

        jLabel32.setFont(new java.awt.Font("GeoSlab703 Md BT", 1, 14)); // NOI18N
        jLabel32.setText("Lama Hari");

        txtlamahari.setFont(new java.awt.Font("Microsoft YaHei Light", 0, 12)); // NOI18N
        txtlamahari.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtlamahariActionPerformed(evt);
            }
        });

        jLabel33.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/search-icon.png"))); // NOI18N

        jLabel34.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/data-add-icon.png"))); // NOI18N
        jLabel34.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel34MouseClicked(evt);
            }
        });

        jLabel36.setFont(new java.awt.Font("GeoSlab703 Md BT", 1, 14)); // NOI18N
        jLabel36.setText("Total Biaya");

        txttotalbiaya.setBackground(new java.awt.Color(204, 0, 0));
        txttotalbiaya.setFont(new java.awt.Font("Microsoft YaHei Light", 0, 12)); // NOI18N
        txttotalbiaya.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txttotalbiayaKeyTyped(evt);
            }
        });

        jLabel37.setFont(new java.awt.Font("GeoSlab703 Md BT", 1, 14)); // NOI18N
        jLabel37.setText("Tambahan Biaya");

        txttambahanbiaya.setFont(new java.awt.Font("Microsoft YaHei Light", 0, 12)); // NOI18N
        txttambahanbiaya.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txttambahanbiayaActionPerformed(evt);
            }
        });

        tambah.setBackground(new java.awt.Color(153, 153, 153));
        tambah.setFont(new java.awt.Font("Franklin Gothic Heavy", 0, 12)); // NOI18N
        tambah.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/data-add-icon.png"))); // NOI18N
        tambah.setText("TAMBAH");
        tambah.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tambahActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(tambah, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel32)
                                    .addComponent(jLabel29)
                                    .addComponent(jLabel28)
                                    .addComponent(jLabel27)
                                    .addComponent(jLabel26))
                                .addGap(44, 44, 44)
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(jPanel4Layout.createSequentialGroup()
                                        .addComponent(txtidpaketwisata, javax.swing.GroupLayout.PREFERRED_SIZE, 173, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jLabel33)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jLabel34))
                                    .addComponent(txtharga)
                                    .addComponent(txtlamahari)
                                    .addComponent(jScrollPane2)
                                    .addComponent(cbtujuan, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                            .addComponent(jLabel37)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel36)
                                    .addComponent(jLabel30))
                                .addGap(59, 59, 59)
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txtjumlahorang)
                                    .addComponent(txttotalbiaya, javax.swing.GroupLayout.DEFAULT_SIZE, 231, Short.MAX_VALUE)
                                    .addComponent(txttambahanbiaya, javax.swing.GroupLayout.DEFAULT_SIZE, 231, Short.MAX_VALUE))))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel26)
                        .addComponent(txtidpaketwisata, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel34)
                    .addComponent(jLabel33))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbtujuan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel27))
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(40, 40, 40)
                        .addComponent(jLabel28)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtharga, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel29))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel32)
                    .addComponent(txtlamahari, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel30)
                    .addComponent(txtjumlahorang, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel37)
                    .addComponent(txttambahanbiaya, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txttotalbiaya, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel36))
                .addGap(21, 21, 21)
                .addComponent(tambah))
        );

        jPanel6.setBackground(new java.awt.Color(0, 153, 153));

        tabeltransaksi.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane3.setViewportView(tabeltransaksi);

        jLabel19.setFont(new java.awt.Font("GeoSlab703 Md BT", 1, 14)); // NOI18N
        jLabel19.setText("ID Nota :");

        jtgl.setModel(new javax.swing.SpinnerDateModel());

        jLabel31.setFont(new java.awt.Font("GeoSlab703 Md BT", 1, 14)); // NOI18N
        jLabel31.setText("Tanggal :");

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 728, Short.MAX_VALUE)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGap(141, 141, 141)
                        .addComponent(jLabel19)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtidnota, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(53, 53, 53)
                        .addComponent(jLabel31)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jtgl, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtidnota, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel19)
                    .addComponent(jtgl, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel31))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 173, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(27, Short.MAX_VALUE))
        );

        simpan.setBackground(new java.awt.Color(153, 153, 153));
        simpan.setFont(new java.awt.Font("Franklin Gothic Heavy", 0, 12)); // NOI18N
        simpan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/Black-Floppy-icon.png"))); // NOI18N
        simpan.setText("SIMPAN");
        simpan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                simpanActionPerformed(evt);
            }
        });

        reset.setBackground(new java.awt.Color(153, 153, 153));
        reset.setFont(new java.awt.Font("Franklin Gothic Heavy", 0, 12)); // NOI18N
        reset.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/Clear-icon.png"))); // NOI18N
        reset.setText("RESET");
        reset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                resetActionPerformed(evt);
            }
        });

        hapus.setBackground(new java.awt.Color(153, 153, 153));
        hapus.setFont(new java.awt.Font("Franklin Gothic Heavy", 0, 12)); // NOI18N
        hapus.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/Button-Close-icon.png"))); // NOI18N
        hapus.setText("DELETE");
        hapus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                hapusActionPerformed(evt);
            }
        });

        jPanel11.setBackground(new java.awt.Color(153, 153, 153));

        jLabel35.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/Button-Close-icon.png"))); // NOI18N
        jLabel35.setToolTipText("CLOSE");
        jLabel35.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel35MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel11Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel35)
                .addContainerGap())
        );
        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel35, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 28, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGap(17, 17, 17)
                                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(simpan)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(reset)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(hapus)
                                .addGap(45, 45, 45))))
                    .addComponent(jPanel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(0, 0, 0))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 221, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(simpan)
                            .addComponent(reset)
                            .addComponent(hapus))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jLabel22MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel22MouseClicked
        new homeuser().show();
        this.dispose();
        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel22MouseClicked

    private void cbtujuanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbtujuanActionPerformed
        String tujuan;
        tujuan = (String) cbtujuan.getSelectedItem();
        if (tujuan == "Bogor"){
            tempat_wisata = "Taman Wisata Rusa, Taman Bunga Nusantara, Little Venice, Curug Cilember, Wisata Oleh-Oleh";

        } else if (tujuan == "Yogyakarta"){
            tempat_wisata = "Taman Bunga Matahari, Pantai Parangtritis, Malioboro, Candi Prambanan, Candi Borobudur";

        } else if (tujuan == "Sukabumi"){
            tempat_wisata = "Pantai Ujung Genteng, Konservasi Penyu, Curug Cikaso, Goa Lalay, Air Panas Cisolok";

        } else if (tujuan == "Bandung"){
            tempat_wisata = "Tangkuban Perahu, Kawah Putih, Ranca Upas, Farm House Susu Lembang , Rumah Mode Bandung";

        } else if (tujuan == "Malang"){
            tempat_wisata = "Gunung Bromo, Tour Jeep, Batu Night Spectacular, Museum Angkut, Petik Apel Khas Malang";

        } else if (tujuan == "Baturaden"){
            tempat_wisata = "Wisata Baturaden, Telaga Sunyi, Kebun Raya Baturaden, Small World Baturaden, Theater Alam";

        } else if (tujuan == "Garut"){
            tempat_wisata = "Candi Cangkuang, Telaga Bodas, Situ Bagendit, Air Panas Cipanas, Kerajinan Kulit Sukarenggong";

        } else if (tujuan == "Pangandaran"){
            tempat_wisata = "Green Canyon, Pantai Pangandaran, Pantai Batu Karas, Wisata Oleh-oleh";

        } else if (tujuan == "Dieng"){
            tempat_wisata = "Kawah Sileri, Telaga Merdada, Air Terjun Sikarim , Wisata Kuliner Dieng";

        } else if (tujuan == "Majalengka"){
            tempat_wisata = "Terasering Panyawengan, Curug Muara Jaya, Curug Sempong";

        }
        txttempatwisata.setText(tempat_wisata);

        // TODO add your handling code here:
    }//GEN-LAST:event_cbtujuanActionPerformed

    private void txtidpelangganActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtidpelangganActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtidpelangganActionPerformed

    private void txttarifbusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txttarifbusActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txttarifbusActionPerformed

    private void cbjenisbusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbjenisbusActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cbjenisbusActionPerformed

    private void txtidbuswisataActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtidbuswisataActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtidbuswisataActionPerformed

    private void jLabel7MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel7MouseClicked
    popuppelanggan Pp = new popuppelanggan();
    Pp.pelanggan = this;
    Pp.setVisible(true);
    Pp.setResizable(false);
        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel7MouseClicked

    private void jLabel7AncestorAdded(javax.swing.event.AncestorEvent evt) {//GEN-FIRST:event_jLabel7AncestorAdded
    
        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel7AncestorAdded

    private void jLabel20MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel20MouseClicked
    popuparmada Pp = new popuparmada();
    Pp.armada = this;
    Pp.setVisible(true);
    Pp.setResizable(false);
        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel20MouseClicked

    private void jLabel34MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel34MouseClicked
    popuppaketwisata Pp = new popuppaketwisata();
    Pp.paketwisata3 = this;
    Pp.setVisible(true);
    Pp.setResizable(false);
        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel34MouseClicked

    private void simpanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_simpanActionPerformed
     SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    String fd = sdf.format(jtgl.getValue());
    String sql = "insert into nota values (?,?,?)";
    String zsql = "insert into isi values (?,?,?,?,?,?,?,?,?,?,?)";
    try{
    PreparedStatement stat = conn.prepareStatement(sql);
    stat.setString(1, txtidnota.getText());
    stat.setString(2, fd);
    stat.setString(3, txtidpelanggan.getText());
    stat.executeUpdate();
    int t = tabeltransaksi.getRowCount();
    for(int i=0; i<t; i++)
    {
    String xidpelanggan = tabeltransaksi.getValueAt(i, 0).toString();
    String xidbuswisata = tabeltransaksi.getValueAt(i, 1).toString();
    String xidpaketwisata = tabeltransaksi.getValueAt(i, 2).toString();
    String xtujuan = tabeltransaksi.getValueAt(i, 3).toString();
    String xtempatwisata = tabeltransaksi.getValueAt(i, 4).toString();
    String xharga = tabeltransaksi.getValueAt(i, 5).toString();
    String xlamahari = tabeltransaksi.getValueAt(i, 6).toString();
    String xjumlahorang = tabeltransaksi.getValueAt(i, 7).toString();
    String xtambahanbiaya = tabeltransaksi.getValueAt(i, 8).toString();
    String xtotalbiaya = tabeltransaksi.getValueAt(i, 9).toString();
    
    PreparedStatement stat2 = conn.prepareStatement(zsql);
    stat2.setString(1, txtidnota.getText());
    stat2.setString(2, xidpelanggan);
    stat2.setString(3, xidbuswisata);
    stat2.setString(4, xidpaketwisata);
    stat2.setString(5, xtujuan);
    stat2.setString(6, xtempatwisata);
    stat2.setString(7, xharga);
    stat2.setString(8, xlamahari);
    stat2.setString(9, xjumlahorang);
    stat2.setString(10, xtambahanbiaya);
    stat2.setString(11, xtotalbiaya);
 
    stat2.executeUpdate();
    }
    JOptionPane.showMessageDialog(null, "Data Berhasil Disimpan");
    
    }
    catch (SQLException e){
    JOptionPane.showMessageDialog(null, "Data Gagal Disimpan"+e);
    }
    cetak();
    kosong();
    aktif();
    autonumber();
    }//GEN-LAST:event_simpanActionPerformed

    private void resetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_resetActionPerformed
    kosong();
    aktif();
    autonumber();    
        // TODO add your handling code here:
    }//GEN-LAST:event_resetActionPerformed

    private void txtjumlahorangActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtjumlahorangActionPerformed
    
        // TODO add your handling code here:
    }//GEN-LAST:event_txtjumlahorangActionPerformed

    private void hapusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_hapusActionPerformed
     int index = tabeltransaksi.getSelectedRow();
    tabmode.removeRow(index);
    tabeltransaksi.setModel(tabmode);
        // TODO add your handling code here:
    }//GEN-LAST:event_hapusActionPerformed

    private void txttambahanbiayaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txttambahanbiayaActionPerformed
    int xjumlahorang,xharga,xtambahanbiaya,xtotal;
        xjumlahorang=Integer.parseInt(txtjumlahorang.getText());
        xharga=Integer.parseInt(txtharga.getText());
        xtambahanbiaya=Integer.parseInt(txttambahanbiaya.getText());
        xtotal=(xjumlahorang*xharga)+xtambahanbiaya;
        txttotalbiaya.setText(String.valueOf(xtotal)); 
        // TODO add your handling code here:
    }//GEN-LAST:event_txttambahanbiayaActionPerformed

    private void tambahActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tambahActionPerformed
    try{
    String idpelanggan = txtidpelanggan.getText();
    String idbuswisata = txtidbuswisata.getText();
    String idpaketwisata = txtidpaketwisata.getText();
    Object tujuan = cbtujuan.getSelectedItem();
    String tempatwisata = txttempatwisata.getText();
    String harga = txtharga.getText();
    int lamahari = Integer.parseInt(txtlamahari.getText());
    int jumlahorang = Integer.parseInt(txtjumlahorang.getText());
    int tambahanbiaya = Integer.parseInt(txttambahanbiaya.getText());
    int totalbiaya = Integer.parseInt(txttotalbiaya.getText());
    
    tabmode.addRow(new Object[]{idpelanggan,idbuswisata,idpaketwisata,tujuan,tempatwisata,harga,lamahari,jumlahorang,tambahanbiaya,totalbiaya});
    tabeltransaksi.setModel(tabmode);
    }
    catch(Exception e)
    {
    System.out.println("Error : "+e);
    }
    txtidpelanggan.setText("");
    txtidbuswisata.setText("");
    txtidpaketwisata.setText("");
    cbtujuan.setSelectedItem(null);
    txttempatwisata.setText("");
    txtharga.setText("");
    txtlamahari.setText("");
    txtjumlahorang.setText("");
    txttambahanbiaya.setText("");
    txttotalbiaya.setText("");
    kosong();
        // TODO add your handling code here:
    }//GEN-LAST:event_tambahActionPerformed

    private void txtlamahariActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtlamahariActionPerformed
                
        // TODO add your handling code here:
    }//GEN-LAST:event_txtlamahariActionPerformed

    private void jLabel35MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel35MouseClicked
        new login().show();
        this.dispose();
        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel35MouseClicked

    private void txttotalbiayaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txttotalbiayaKeyTyped
    int xjumlahorang,xharga,xtambahanbiaya,xtotal;
        xjumlahorang=Integer.parseInt(txtjumlahorang.getText());
        xharga=Integer.parseInt(txtharga.getText());
        xtambahanbiaya=Integer.parseInt(txttambahanbiaya.getText());
        xtotal=(xjumlahorang*xharga)+xtambahanbiaya;
        txttotalbiaya.setText(String.valueOf(xtotal));
        // TODO add your handling code here:
    }//GEN-LAST:event_txttotalbiayaKeyTyped

    private void jLabel21MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel21MouseClicked
        new laporan().show();
        this.dispose();
        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel21MouseClicked

    private void jLabel23MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel23MouseClicked
        new login().show();
        this.dispose();   
        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel23MouseClicked
    
     
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        
    
    
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(pemesanan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(pemesanan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(pemesanan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(pemesanan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new pemesanan().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JComboBox cbjenisbus;
    private javax.swing.JComboBox cbtujuan;
    private javax.swing.JButton hapus;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSpinner jtgl;
    private javax.swing.JButton reset;
    private javax.swing.JButton simpan;
    private javax.swing.JTable tabeltransaksi;
    private javax.swing.JButton tambah;
    private javax.swing.JTextArea txtalamat;
    private javax.swing.JTextField txtharga;
    private javax.swing.JTextField txtidbuswisata;
    private javax.swing.JTextField txtidnota;
    private javax.swing.JTextField txtidpaketwisata;
    private javax.swing.JTextField txtidpelanggan;
    private javax.swing.JTextField txtjumlahorang;
    private javax.swing.JTextField txtlamahari;
    private javax.swing.JTextField txtnama;
    private javax.swing.JTextField txtnamabus;
    private javax.swing.JTextField txtnotelp;
    private javax.swing.JTextField txttambahanbiaya;
    private javax.swing.JTextField txttarifbus;
    private javax.swing.JTextArea txttempatwisata;
    private javax.swing.JTextField txttotalbiaya;
    // End of variables declaration//GEN-END:variables
}
