/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package sisteminformasipaketwisata;
import java.awt.event.KeyEvent;
import java.io.File;
import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import koneksi.koneksi;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JasperViewer;


/**
 *
 * @author ASUS
 */
public class laporan extends javax.swing.JFrame {
private Connection conn = new koneksi().connect();
private DefaultTableModel tabmode;
int i=0;

    /**
     * Creates new form pelanggan
     */
    public laporan() {
        initComponents();
        datatable_pelanggan();
        this.setLocationRelativeTo(null);
        new Thread(){
            public void run(){
                while(true){
                    Calendar kal = new GregorianCalendar();
                    int tahun = kal.get (Calendar.YEAR);
                    int bulan = kal.get (Calendar.MONTH)+1;
                    int hari = kal.get (Calendar.DAY_OF_MONTH);
                    int jam = kal.get (Calendar.HOUR_OF_DAY);
                    int menit = kal.get (Calendar.MINUTE);
                    int detik = kal.get (Calendar.SECOND);
                    String tanggal = tahun+"-"+bulan+"-"+hari;
                    String waktu1 = jam+":"+menit+":"+detik;
                    String waktu2 = jam+"-"+menit+"-"+detik;
                    jLabel3.setText(tanggal);
                    jLabel4.setText(waktu1);
                }
            }
        }.start();

    }
    
    public void removeTable(){
        try{
            for (int t=tabmode.getRowCount(); t>0; t--) {tabmode.removeRow(0);}
        }catch (Exception ex) {
            System.out.println(ex);
        }
    }
    
    private void kosong(){
        txtidpelanggan.setText("");
        txtidpaketwisata.setText("");
        txtidbus.setText("");
        txtidpembayaran.setText("");
        txtcaripelanggan.setText("");
        txtcaripaketwisata.setText("");
        txtcaribus.setText("");
        txtcaripembayaran.setText("");
        
    }
   
    protected void datatable_pelanggan(){
        Object[]Baris={"ID Pelanggan","Nama","No KTP","Jenis Kelamin","Alamat","No Telfon","Email"};
        tabmode = new DefaultTableModel(null,Baris);
        String cari_data = txtcaripelanggan.getText();
        
        try{
            String query = "select * from pelanggan where nama like '%" +cari_data+"%'"
                        +"order by idpelanggan asc";
            java.sql.Statement statment = conn.createStatement();
            ResultSet data = statment.executeQuery(query);
            while (data.next()){
                tabmode.addRow(new Object[]{
                    data.getString(1),
                    data.getString(2),
                    data.getString(3),
                    data.getString(4),
                    data.getString(5),
                    data.getString(6),
                    data.getString(7)
                });
            }
            tabelpelanggan.setModel(tabmode);
        }catch(Exception e){
             JOptionPane.showMessageDialog(null,"Data Gagal Masuk Tabel {"+e+"}");
        }
    }
    
    protected void datatable_paketwisata(){
        Object[]Baris={"ID Paket Wisata","Tujuan","Tempat Wisata","Harga Per Pack","Makan","Lama Hari","Tanggal Berangkat"};
        tabmode = new DefaultTableModel(null,Baris);
        String cari_data = txtcaripaketwisata.getText();
        
        try{
            String query = "select * from paketwisata where idpaketwisata like '%" +cari_data+"%'"
                        +"order by idpaketwisata asc";
            java.sql.Statement statment = conn.createStatement();
            ResultSet data = statment.executeQuery(query);
            while (data.next()){
                tabmode.addRow(new Object[]{
                    data.getString(1),
                    data.getString(2),
                    data.getString(3),
                    data.getString(4),
                    data.getString(5),
                    data.getString(6),
                    data.getString(7)
                });
            }
            tabelpaketwisata.setModel(tabmode);
        }catch(Exception e){
             JOptionPane.showMessageDialog(null,"Data Gagal Masuk Tabel {"+e+"}");
        }
    }
    
    protected void datatable_bus(){
        Object[]Baris={"ID Bus","Contact Person Supir","Nama Bus","Jenis Bus","Tarif Bus"};
        tabmode = new DefaultTableModel(null,Baris);
        String cari_data = txtcaribus.getText();
        
        try{
            String query = "select * from armada where idbuswisata like '%" +cari_data+"%'"
                        +"order by idbuswisata asc";
            java.sql.Statement statment = conn.createStatement();
            ResultSet data = statment.executeQuery(query);
            while (data.next()){
                tabmode.addRow(new Object[]{
                    data.getString(1),
                    data.getString(2),
                    data.getString(3),
                    data.getString(4),
                    data.getString(5)
                });
            }
            tabelbuswisata.setModel(tabmode);
        }catch(Exception e){
             JOptionPane.showMessageDialog(null,"Data Gagal Masuk Tabel {"+e+"}");
        }
    }
    
   
    
    protected void datatable_pembayaran(){
        Object[]Baris={"ID Pembayaran","Nama","Tanggal Bayar","Guna Bayar","Jumlah Bayar"};
        tabmode = new DefaultTableModel(null,Baris);
        String cari_data = txtcaripembayaran.getText();
        
        try{
            String query = "select * from pembayaran where nama like '%" +cari_data+"%'"
                        +"order by idpembayaran asc";
            java.sql.Statement statment = conn.createStatement();
            ResultSet data = statment.executeQuery(query);
            while (data.next()){
                tabmode.addRow(new Object[]{
                    data.getString(1),
                    data.getString(2),
                    data.getString(3),
                    data.getString(4),
                    data.getString(5)
  
                });
            }
            tabelpembayaran.setModel(tabmode);
        }catch(Exception e){
             JOptionPane.showMessageDialog(null,"Data Gagal Masuk Tabel {"+e+"}");
        }
    }
    
    
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        tabpane = new javax.swing.JTabbedPane();
        panelpelanggan = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabelpelanggan = new javax.swing.JTable();
        lbljum = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        txtcaripelanggan = new javax.swing.JTextField();
        bcaripelanggan = new javax.swing.JButton();
        jPanel7 = new javax.swing.JPanel();
        jLabel27 = new javax.swing.JLabel();
        txtidpelanggan = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        jLabel28 = new javax.swing.JLabel();
        panelpaketwisata = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jLabel12 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tabelpaketwisata = new javax.swing.JTable();
        lbljum2 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        txtcaripaketwisata = new javax.swing.JTextField();
        bcaripaketwisata = new javax.swing.JButton();
        jPanel8 = new javax.swing.JPanel();
        jLabel29 = new javax.swing.JLabel();
        txtidpaketwisata = new javax.swing.JTextField();
        jLabel30 = new javax.swing.JLabel();
        jLabel31 = new javax.swing.JLabel();
        panelbus = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        jLabel13 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tabelbuswisata = new javax.swing.JTable();
        lbljum3 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        txtcaribus = new javax.swing.JTextField();
        bcaribus = new javax.swing.JButton();
        jPanel9 = new javax.swing.JPanel();
        jLabel32 = new javax.swing.JLabel();
        txtidbus = new javax.swing.JTextField();
        jLabel33 = new javax.swing.JLabel();
        jLabel34 = new javax.swing.JLabel();
        paneltravelreguler1 = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jScrollPane5 = new javax.swing.JScrollPane();
        tabelpembayaran = new javax.swing.JTable();
        lbljum5 = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        txtcaripembayaran = new javax.swing.JTextField();
        bcaripembayaran = new javax.swing.JButton();
        jPanel11 = new javax.swing.JPanel();
        jLabel38 = new javax.swing.JLabel();
        txtidpembayaran = new javax.swing.JTextField();
        jLabel39 = new javax.swing.JLabel();
        jLabel40 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jPanel12 = new javax.swing.JPanel();
        jLabel42 = new javax.swing.JLabel();
        jLabel41 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);

        tabpane.setBackground(new java.awt.Color(0, 153, 153));
        tabpane.setFont(new java.awt.Font("GeoSlab703 Md BT", 0, 12)); // NOI18N

        panelpelanggan.setBackground(new java.awt.Color(102, 102, 102));

        jPanel2.setBackground(new java.awt.Color(255, 153, 0));

        jLabel11.setFont(new java.awt.Font("Swis721 Cn BT", 1, 14)); // NOI18N
        jLabel11.setText(" DATA PELANGGAN ");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(527, 527, 527)
                .addComponent(jLabel11)
                .addContainerGap(595, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel11)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        tabelpelanggan.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tabelpelanggan.addAncestorListener(new javax.swing.event.AncestorListener() {
            public void ancestorMoved(javax.swing.event.AncestorEvent evt) {
            }
            public void ancestorAdded(javax.swing.event.AncestorEvent evt) {
                tabelpelangganAncestorAdded(evt);
            }
            public void ancestorRemoved(javax.swing.event.AncestorEvent evt) {
            }
        });
        tabelpelanggan.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabelpelangganMouseClicked(evt);
            }
        });
        tabelpelanggan.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                tabelpelangganPropertyChange(evt);
            }
        });
        jScrollPane1.setViewportView(tabelpelanggan);

        lbljum.setFont(new java.awt.Font("GeoSlab703 Md BT", 0, 14)); // NOI18N
        lbljum.setText("jLabel7");

        jLabel7.setFont(new java.awt.Font("GeoSlab703 Md BT", 0, 14)); // NOI18N
        jLabel7.setText("Jumlah Data :");

        jLabel21.setFont(new java.awt.Font("GeoSlab703 Md BT", 1, 14)); // NOI18N
        jLabel21.setText("Nama Pelanggan");

        txtcaripelanggan.setFont(new java.awt.Font("Microsoft YaHei Light", 0, 12)); // NOI18N
        txtcaripelanggan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtcaripelangganKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtcaripelangganKeyTyped(evt);
            }
        });

        bcaripelanggan.setFont(new java.awt.Font("Franklin Gothic Heavy", 0, 12)); // NOI18N
        bcaripelanggan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/search-icon.png"))); // NOI18N
        bcaripelanggan.setText("CARI");
        bcaripelanggan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bcaripelangganActionPerformed(evt);
            }
        });

        jPanel7.setBackground(new java.awt.Color(255, 153, 0));

        jLabel27.setFont(new java.awt.Font("GeoSlab703 Md BT", 1, 14)); // NOI18N
        jLabel27.setText("ID Pelanggan");

        txtidpelanggan.setFont(new java.awt.Font("Microsoft YaHei Light", 0, 12)); // NOI18N
        txtidpelanggan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtidpelangganKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtidpelangganKeyTyped(evt);
            }
        });

        jLabel20.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/Printer-icon (1).png"))); // NOI18N
        jLabel20.setToolTipText("CETAK DATA");
        jLabel20.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel20MouseClicked(evt);
            }
        });

        jLabel28.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/Hardware-Printer-Floppy-icon.png"))); // NOI18N
        jLabel28.setToolTipText("CETAK LAPORAN");
        jLabel28.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel28MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel27)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtidpelanggan, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel20)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel28)
                .addContainerGap())
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel28)
                    .addComponent(jLabel20)
                    .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtidpelanggan, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel27)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout panelpelangganLayout = new javax.swing.GroupLayout(panelpelanggan);
        panelpelanggan.setLayout(panelpelangganLayout);
        panelpelangganLayout.setHorizontalGroup(
            panelpelangganLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelpelangganLayout.createSequentialGroup()
                .addGap(61, 61, 61)
                .addGroup(panelpelangganLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelpelangganLayout.createSequentialGroup()
                        .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel7)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lbljum)
                        .addGap(71, 71, 71))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelpelangganLayout.createSequentialGroup()
                        .addGroup(panelpelangganLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(panelpelangganLayout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(jLabel21)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtcaripelanggan, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(bcaripelanggan))
                            .addComponent(jScrollPane1))
                        .addGap(60, 60, 60))))
        );
        panelpelangganLayout.setVerticalGroup(
            panelpelangganLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelpelangganLayout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelpelangganLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtcaripelanggan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel21)
                    .addComponent(bcaripelanggan))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 305, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelpelangganLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(panelpelangganLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lbljum)
                        .addComponent(jLabel7)))
                .addContainerGap())
        );

        tabpane.addTab("Data Pelanggan", panelpelanggan);

        panelpaketwisata.setBackground(new java.awt.Color(102, 102, 102));

        jPanel3.setBackground(new java.awt.Color(255, 153, 0));

        jLabel12.setFont(new java.awt.Font("Swis721 Cn BT", 1, 14)); // NOI18N
        jLabel12.setText(" DATA PAKET WISATA ");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(527, 527, 527)
                .addComponent(jLabel12)
                .addContainerGap(577, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel12)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        tabelpaketwisata.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tabelpaketwisata.addAncestorListener(new javax.swing.event.AncestorListener() {
            public void ancestorMoved(javax.swing.event.AncestorEvent evt) {
            }
            public void ancestorAdded(javax.swing.event.AncestorEvent evt) {
                tabelpaketwisataAncestorAdded(evt);
            }
            public void ancestorRemoved(javax.swing.event.AncestorEvent evt) {
            }
        });
        tabelpaketwisata.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                tabelpaketwisataKeyTyped(evt);
            }
        });
        jScrollPane2.setViewportView(tabelpaketwisata);

        lbljum2.setFont(new java.awt.Font("GeoSlab703 Md BT", 0, 14)); // NOI18N
        lbljum2.setText("jLabel7");

        jLabel8.setFont(new java.awt.Font("GeoSlab703 Md BT", 0, 14)); // NOI18N
        jLabel8.setText("Jumlah Data : ");

        jLabel22.setFont(new java.awt.Font("GeoSlab703 Md BT", 1, 14)); // NOI18N
        jLabel22.setText("ID Paket Wisata");

        txtcaripaketwisata.setFont(new java.awt.Font("Microsoft YaHei Light", 0, 12)); // NOI18N
        txtcaripaketwisata.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtcaripaketwisataKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtcaripaketwisataKeyTyped(evt);
            }
        });

        bcaripaketwisata.setFont(new java.awt.Font("Franklin Gothic Heavy", 0, 12)); // NOI18N
        bcaripaketwisata.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/search-icon.png"))); // NOI18N
        bcaripaketwisata.setText("CARI");
        bcaripaketwisata.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bcaripaketwisataActionPerformed(evt);
            }
        });

        jPanel8.setBackground(new java.awt.Color(255, 153, 0));

        jLabel29.setFont(new java.awt.Font("GeoSlab703 Md BT", 1, 14)); // NOI18N
        jLabel29.setText("ID Paket Wisata");

        txtidpaketwisata.setFont(new java.awt.Font("Microsoft YaHei Light", 0, 12)); // NOI18N
        txtidpaketwisata.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtidpaketwisataKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtidpaketwisataKeyTyped(evt);
            }
        });

        jLabel30.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/Printer-icon (1).png"))); // NOI18N
        jLabel30.setToolTipText("CETAK DATA");
        jLabel30.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel30MouseClicked(evt);
            }
        });

        jLabel31.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/Hardware-Printer-Floppy-icon.png"))); // NOI18N
        jLabel31.setToolTipText("CETAK LAPORAN");
        jLabel31.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel31MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel29, javax.swing.GroupLayout.DEFAULT_SIZE, 114, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtidpaketwisata, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel30)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel31)
                .addContainerGap())
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel31)
                    .addComponent(jLabel30)
                    .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtidpaketwisata, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel29)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout panelpaketwisataLayout = new javax.swing.GroupLayout(panelpaketwisata);
        panelpaketwisata.setLayout(panelpaketwisataLayout);
        panelpaketwisataLayout.setHorizontalGroup(
            panelpaketwisataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelpaketwisataLayout.createSequentialGroup()
                .addGroup(panelpaketwisataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(panelpaketwisataLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jLabel22)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtcaripaketwisata, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(bcaripaketwisata))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, panelpaketwisataLayout.createSequentialGroup()
                        .addGap(61, 61, 61)
                        .addGroup(panelpaketwisataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelpaketwisataLayout.createSequentialGroup()
                                .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel8)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lbljum2))
                            .addComponent(jScrollPane2))))
                .addGap(60, 60, 60))
        );
        panelpaketwisataLayout.setVerticalGroup(
            panelpaketwisataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelpaketwisataLayout.createSequentialGroup()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelpaketwisataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtcaripaketwisata, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel22)
                    .addComponent(bcaripaketwisata))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 305, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelpaketwisataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lbljum2)
                    .addComponent(jLabel8)
                    .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        tabpane.addTab("Data Paket Wisata", panelpaketwisata);

        panelbus.setBackground(new java.awt.Color(102, 102, 102));

        jPanel5.setBackground(new java.awt.Color(255, 153, 0));

        jLabel13.setFont(new java.awt.Font("Swis721 Cn BT", 1, 14)); // NOI18N
        jLabel13.setText(" DATA BUS WISATA ");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(527, 527, 527)
                .addComponent(jLabel13)
                .addContainerGap(591, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel13)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel9.setFont(new java.awt.Font("GeoSlab703 Md BT", 0, 14)); // NOI18N
        jLabel9.setText("Jumlah Data :");

        tabelbuswisata.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tabelbuswisata.addAncestorListener(new javax.swing.event.AncestorListener() {
            public void ancestorMoved(javax.swing.event.AncestorEvent evt) {
            }
            public void ancestorAdded(javax.swing.event.AncestorEvent evt) {
                tabelbuswisataAncestorAdded(evt);
            }
            public void ancestorRemoved(javax.swing.event.AncestorEvent evt) {
            }
        });
        jScrollPane3.setViewportView(tabelbuswisata);

        lbljum3.setFont(new java.awt.Font("GeoSlab703 Md BT", 0, 14)); // NOI18N
        lbljum3.setText("jLabel11");

        jLabel23.setFont(new java.awt.Font("GeoSlab703 Md BT", 1, 14)); // NOI18N
        jLabel23.setText("ID Bus");

        txtcaribus.setFont(new java.awt.Font("Microsoft YaHei Light", 0, 12)); // NOI18N
        txtcaribus.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtcaribusKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtcaribusKeyTyped(evt);
            }
        });

        bcaribus.setFont(new java.awt.Font("Franklin Gothic Heavy", 0, 12)); // NOI18N
        bcaribus.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/search-icon.png"))); // NOI18N
        bcaribus.setText("CARI");
        bcaribus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bcaribusActionPerformed(evt);
            }
        });

        jPanel9.setBackground(new java.awt.Color(255, 153, 0));

        jLabel32.setFont(new java.awt.Font("GeoSlab703 Md BT", 1, 14)); // NOI18N
        jLabel32.setText("ID Bus");

        txtidbus.setFont(new java.awt.Font("Microsoft YaHei Light", 0, 12)); // NOI18N
        txtidbus.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtidbusKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtidbusKeyTyped(evt);
            }
        });

        jLabel33.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/Printer-icon (1).png"))); // NOI18N
        jLabel33.setToolTipText("CETAK DATA");
        jLabel33.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel33MouseClicked(evt);
            }
        });

        jLabel34.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/Hardware-Printer-Floppy-icon.png"))); // NOI18N
        jLabel34.setToolTipText("CETAK LAPORAN");
        jLabel34.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel34MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel9Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel32)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtidbus, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel33)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel34)
                .addContainerGap())
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel34)
                    .addComponent(jLabel33)
                    .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtidbus, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel32)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout panelbusLayout = new javax.swing.GroupLayout(panelbus);
        panelbus.setLayout(panelbusLayout);
        panelbusLayout.setHorizontalGroup(
            panelbusLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelbusLayout.createSequentialGroup()
                .addGroup(panelbusLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(panelbusLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jLabel23)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtcaribus, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(bcaribus))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, panelbusLayout.createSequentialGroup()
                        .addGap(61, 61, 61)
                        .addGroup(panelbusLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelbusLayout.createSequentialGroup()
                                .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel9)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(lbljum3))
                            .addComponent(jScrollPane3))))
                .addGap(60, 60, 60))
        );
        panelbusLayout.setVerticalGroup(
            panelbusLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelbusLayout.createSequentialGroup()
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelbusLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtcaribus, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel23)
                    .addComponent(bcaribus))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 305, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelbusLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelbusLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lbljum3)
                        .addComponent(jLabel9))
                    .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        tabpane.addTab("Data Bus ", panelbus);

        paneltravelreguler1.setBackground(new java.awt.Color(102, 102, 102));

        jPanel6.setBackground(new java.awt.Color(255, 153, 0));

        jLabel15.setFont(new java.awt.Font("Swis721 Cn BT", 1, 14)); // NOI18N
        jLabel15.setText(" DATA PEMBAYARAN ");

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(527, 527, 527)
                .addComponent(jLabel15)
                .addContainerGap(583, Short.MAX_VALUE))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel15)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel16.setFont(new java.awt.Font("GeoSlab703 Md BT", 0, 14)); // NOI18N
        jLabel16.setText("Jumlah Data :");

        tabelpembayaran.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tabelpembayaran.addAncestorListener(new javax.swing.event.AncestorListener() {
            public void ancestorMoved(javax.swing.event.AncestorEvent evt) {
            }
            public void ancestorAdded(javax.swing.event.AncestorEvent evt) {
                tabelpembayaranAncestorAdded(evt);
            }
            public void ancestorRemoved(javax.swing.event.AncestorEvent evt) {
            }
        });
        jScrollPane5.setViewportView(tabelpembayaran);

        lbljum5.setFont(new java.awt.Font("GeoSlab703 Md BT", 0, 14)); // NOI18N
        lbljum5.setText("jLabel11");

        jLabel26.setFont(new java.awt.Font("GeoSlab703 Md BT", 1, 14)); // NOI18N
        jLabel26.setText("Nama Pelanggan");

        txtcaripembayaran.setFont(new java.awt.Font("Microsoft YaHei Light", 0, 12)); // NOI18N
        txtcaripembayaran.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtcaripembayaranKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtcaripembayaranKeyTyped(evt);
            }
        });

        bcaripembayaran.setFont(new java.awt.Font("Franklin Gothic Heavy", 0, 12)); // NOI18N
        bcaripembayaran.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/search-icon.png"))); // NOI18N
        bcaripembayaran.setText("CARI");
        bcaripembayaran.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bcaripembayaranActionPerformed(evt);
            }
        });

        jPanel11.setBackground(new java.awt.Color(255, 153, 0));

        jLabel38.setFont(new java.awt.Font("GeoSlab703 Md BT", 1, 14)); // NOI18N
        jLabel38.setText("ID Pembayaran");

        txtidpembayaran.setFont(new java.awt.Font("Microsoft YaHei Light", 0, 12)); // NOI18N
        txtidpembayaran.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtidpembayaranKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtidpembayaranKeyTyped(evt);
            }
        });

        jLabel39.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/Printer-icon (1).png"))); // NOI18N
        jLabel39.setToolTipText("CETAK DATA");
        jLabel39.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel39MouseClicked(evt);
            }
        });

        jLabel40.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/Hardware-Printer-Floppy-icon.png"))); // NOI18N
        jLabel40.setToolTipText("CETAK LAPORAN");
        jLabel40.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel40MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel11Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel38)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtidpembayaran, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel39)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel40)
                .addContainerGap())
        );
        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel40)
                    .addComponent(jLabel39)
                    .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtidpembayaran, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel38)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout paneltravelreguler1Layout = new javax.swing.GroupLayout(paneltravelreguler1);
        paneltravelreguler1.setLayout(paneltravelreguler1Layout);
        paneltravelreguler1Layout.setHorizontalGroup(
            paneltravelreguler1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, paneltravelreguler1Layout.createSequentialGroup()
                .addGroup(paneltravelreguler1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(paneltravelreguler1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jLabel26)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtcaripembayaran, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(bcaripembayaran))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, paneltravelreguler1Layout.createSequentialGroup()
                        .addGap(61, 61, 61)
                        .addGroup(paneltravelreguler1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(paneltravelreguler1Layout.createSequentialGroup()
                                .addComponent(jPanel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel16)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(lbljum5))
                            .addComponent(jScrollPane5))))
                .addGap(60, 60, 60))
        );
        paneltravelreguler1Layout.setVerticalGroup(
            paneltravelreguler1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(paneltravelreguler1Layout.createSequentialGroup()
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(paneltravelreguler1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtcaripembayaran, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel26)
                    .addComponent(bcaripembayaran))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 305, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(paneltravelreguler1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(paneltravelreguler1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lbljum5)
                        .addComponent(jLabel16))
                    .addComponent(jPanel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        tabpane.addTab("Data  Pembayaran", paneltravelreguler1);

        jLabel19.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/previous.png"))); // NOI18N
        jLabel19.setToolTipText("BACK");
        jLabel19.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel19MouseClicked(evt);
            }
        });

        jLabel18.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/next.png"))); // NOI18N
        jLabel18.setToolTipText("NEXT");
        jLabel18.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel18MouseClicked(evt);
            }
        });

        jPanel12.setBackground(new java.awt.Color(153, 153, 153));

        jLabel42.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/Button-Close-icon.png"))); // NOI18N
        jLabel42.setToolTipText("CLOSE");
        jLabel42.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel42MouseClicked(evt);
            }
        });

        jLabel41.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/Home-icon (2).png"))); // NOI18N
        jLabel41.setToolTipText("HOME");
        jLabel41.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel41MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel12Layout = new javax.swing.GroupLayout(jPanel12);
        jPanel12.setLayout(jPanel12Layout);
        jPanel12Layout.setHorizontalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel12Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel41)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel42)
                .addContainerGap())
        );
        jPanel12Layout.setVerticalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel42, javax.swing.GroupLayout.DEFAULT_SIZE, 29, Short.MAX_VALUE)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addComponent(jLabel41)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        jPanel1.setBackground(new java.awt.Color(0, 51, 51));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/logo travel report.png"))); // NOI18N
        jLabel1.setMaximumSize(new java.awt.Dimension(110, 110));
        jLabel1.setMinimumSize(new java.awt.Dimension(110, 110));
        jLabel1.setPreferredSize(new java.awt.Dimension(50, 50));

        jLabel2.setFont(new java.awt.Font("News701 BT", 1, 12)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Jl. Penjalin No.2, RT 07/03, Desa Pasirsari, Cikarang Selatan, Bekasi, 17550");

        jLabel3.setBackground(new java.awt.Color(255, 255, 255));
        jLabel3.setFont(new java.awt.Font("Times New Roman", 2, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Tanggal");

        jLabel4.setBackground(new java.awt.Color(255, 255, 255));
        jLabel4.setFont(new java.awt.Font("Times New Roman", 2, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Waktu");

        jLabel5.setBackground(new java.awt.Color(255, 255, 255));
        jLabel5.setFont(new java.awt.Font("Times New Roman", 2, 14)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Tanggal :");

        jLabel6.setBackground(new java.awt.Color(255, 255, 255));
        jLabel6.setFont(new java.awt.Font("Times New Roman", 2, 14)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("Waktu :");

        jLabel24.setFont(new java.awt.Font("News701 BT", 1, 24)); // NOI18N
        jLabel24.setForeground(new java.awt.Color(255, 255, 255));
        jLabel24.setText("SISTEM INFORMASI PEMESANAN PAKET WISATA");

        jLabel17.setFont(new java.awt.Font("News701 BT", 1, 18)); // NOI18N
        jLabel17.setForeground(new java.awt.Color(255, 255, 255));
        jLabel17.setText("GOLDEN PRIMA TOUR AND TRAVEL");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(54, 54, 54)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(24, 24, 24)
                        .addComponent(jLabel24, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(142, 142, 142)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addGap(34, 34, 34)
                                .addComponent(jLabel17, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGap(192, 192, 192))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGap(165, 165, 165)))))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel6)
                    .addComponent(jLabel5))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4)
                    .addComponent(jLabel3))
                .addGap(57, 57, 57))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(5, 5, 5)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(jLabel3)
                            .addComponent(jLabel24))
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(25, 25, 25)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel4)
                                    .addComponent(jLabel6)))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel17)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel2)))))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel19)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel18)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addComponent(jPanel12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(tabpane, javax.swing.GroupLayout.Alignment.TRAILING)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(tabpane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel18)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel19)
                        .addGap(0, 0, 0))))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void tabelpelangganPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_tabelpelangganPropertyChange
    // TODO add your handling code here:
    }//GEN-LAST:event_tabelpelangganPropertyChange

    private void tabelpelangganAncestorAdded(javax.swing.event.AncestorEvent evt) {//GEN-FIRST:event_tabelpelangganAncestorAdded
     removeTable();
        int jumdata=0;
        Object[]Baris={"ID Pelanggan","Nama","No KTP","Jenis Kelamin","Alamat","No Telfon","Email"};
        tabmode = new DefaultTableModel(null,Baris);
        tabelpelanggan.setModel(tabmode);
        String sql = "Select * from pelanggan";
        try{
            java.sql.Statement stat = conn.createStatement();
            ResultSet hasil = stat.executeQuery(sql);
            while(hasil.next()){
            String idpelanggan = hasil.getString("idpelanggan");
            String nama = hasil.getString("nama");
            String noktp = hasil.getString("noktp");
            String jeniskelamin = hasil.getString("jkel");
            String alamat = hasil.getString("alamat");
            String notelp = hasil.getString("notelp");
            String email = hasil.getString("email");
            
            String[] data = {idpelanggan,nama,noktp,jeniskelamin,alamat,notelp,email};
            tabmode.addRow(data);
            jumdata = jumdata+1;
            }
        }catch (SQLException sqle){
            jumdata=0;
            JOptionPane.showMessageDialog(null,"Data Gagal Masuk Tabel"+sqle);
        }
        lbljum.setText(jumdata+" Item");
    
        // TODO add your handling code here:
    }//GEN-LAST:event_tabelpelangganAncestorAdded

    private void tabelbuswisataAncestorAdded(javax.swing.event.AncestorEvent evt) {//GEN-FIRST:event_tabelbuswisataAncestorAdded
    removeTable();
        int jumdata=0;
        Object[]Baris={"ID Bus","Contact Person Supir","Nama Bus","Jenis Bus","Tarif Bus"};
        tabmode = new DefaultTableModel(null,Baris);
        tabelbuswisata.setModel(tabmode);
        String sql = "Select * from armada";
        try{
            java.sql.Statement stat = conn.createStatement();
            ResultSet hasil = stat.executeQuery(sql);
            while(hasil.next()){
            String idbuswisata = hasil.getString("idbuswisata");
            String notelp = hasil.getString("notelp");
            String namabus = hasil.getString("namabus");
            String jenisbus = hasil.getString("jenisbus");
            String tarifbus = hasil.getString("tarifbus");

                     
            String[] data = {idbuswisata,notelp,namabus,jenisbus,tarifbus};
            tabmode.addRow(data);
            jumdata = jumdata+1;
            }
        }catch (SQLException sqle){
            jumdata=0;
            JOptionPane.showMessageDialog(null,"Data Gagal Masuk Tabel"+sqle);
        }
        lbljum3.setText(jumdata+" Item");
        // TODO add your handling code here:*/
    }//GEN-LAST:event_tabelbuswisataAncestorAdded

    private void tabelpaketwisataAncestorAdded(javax.swing.event.AncestorEvent evt) {//GEN-FIRST:event_tabelpaketwisataAncestorAdded
        removeTable();
        int jumdata=0;
         Object[]Baris={"ID Paket Wisata","Tujuan","Tempat Wisata","Harga Per Pack","Makan","Lama Hari","Tanggal Berangkat"};
        tabmode = new DefaultTableModel(null,Baris);
        tabelpaketwisata.setModel(tabmode);
        String sql = "Select * from paketwisata";
        try{
            java.sql.Statement stat = conn.createStatement();
            ResultSet hasil = stat.executeQuery(sql);
            while(hasil.next()){
                String idpaketwisata = hasil.getString("idpaketwisata");
                String tujuan = hasil.getString("tujuan");
                String tempatwisata = hasil.getString("tempatwisata");
                String hargaperorang = hasil.getString("hargaperorang");
                String makan = hasil.getString("makan");
                String lamahari = hasil.getString("lamahari");
                String tglberangkat = hasil.getString("tglberangkat");

                String[] data = {idpaketwisata,tujuan,tempatwisata,hargaperorang,makan,lamahari,tglberangkat};
                tabmode.addRow(data);
                jumdata = jumdata+1;
            }
        }catch (SQLException sqle){
            jumdata=0;
            JOptionPane.showMessageDialog(null,"Data Gagal Masuk Tabel"+sqle);
        }
        lbljum2.setText(jumdata+" Item");

        // TODO add your handling code here:
    }//GEN-LAST:event_tabelpaketwisataAncestorAdded

    private void jLabel18MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel18MouseClicked
    if (i>=4) JOptionPane.showMessageDialog(null,"Tab Sudah yang terakhir");
      else i++;
        tabpane.setSelectedIndex(i);
        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel18MouseClicked

    private void jLabel19MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel19MouseClicked
    if (i<=0) JOptionPane.showMessageDialog(null,"Tab Sudah yang pertama");
    else i--;
        tabpane.setSelectedIndex(i);
        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel19MouseClicked

    private void bcaripelangganActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bcaripelangganActionPerformed

        datatable_pelanggan();
        kosong();
    }//GEN-LAST:event_bcaripelangganActionPerformed

    private void txtcaripelangganKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtcaripelangganKeyTyped
        // TODO add your handling code here:
        datatable_pelanggan();
    }//GEN-LAST:event_txtcaripelangganKeyTyped

    private void tabelpelangganMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabelpelangganMouseClicked
        // TODO add your handling code here:
        removeTable();
    }//GEN-LAST:event_tabelpelangganMouseClicked

    private void txtcaripaketwisataKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtcaripaketwisataKeyTyped
        // TODO add your handling code here:
        datatable_paketwisata();
    }//GEN-LAST:event_txtcaripaketwisataKeyTyped

    private void bcaripaketwisataActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bcaripaketwisataActionPerformed
        // TODO add your handling code here:
        datatable_paketwisata();
        kosong();
    }//GEN-LAST:event_bcaripaketwisataActionPerformed

    private void tabelpaketwisataKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tabelpaketwisataKeyTyped

        // TODO add your handling code here:
    }//GEN-LAST:event_tabelpaketwisataKeyTyped

    private void txtcaribusKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtcaribusKeyTyped
        // TODO add your handling code here:
        datatable_bus();
    }//GEN-LAST:event_txtcaribusKeyTyped

    private void bcaribusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bcaribusActionPerformed
        // TODO add your handling code here:
        datatable_bus();
        kosong();
    }//GEN-LAST:event_bcaribusActionPerformed

    private void txtcaripaketwisataKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtcaripaketwisataKeyPressed
        // TODO add your handling code here:
        datatable_paketwisata();
    }//GEN-LAST:event_txtcaripaketwisataKeyPressed

    private void txtcaripelangganKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtcaripelangganKeyPressed
        // TODO add your handling code here:
        datatable_pelanggan();
    }//GEN-LAST:event_txtcaripelangganKeyPressed

    private void txtcaribusKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtcaribusKeyPressed
        // TODO add your handling code here:
        datatable_pelanggan();
    }//GEN-LAST:event_txtcaribusKeyPressed

    private void tabelpembayaranAncestorAdded(javax.swing.event.AncestorEvent evt) {//GEN-FIRST:event_tabelpembayaranAncestorAdded
    removeTable();
        int jumdata=0;
        Object[]Baris={"ID Pembayaran","Nama","Tanggal Bayar","Guna Bayar","Jumlah Bayar"};
        tabmode = new DefaultTableModel(null,Baris);
        tabelpembayaran.setModel(tabmode);
        String sql = "Select * from pembayaran";
        try{
            java.sql.Statement stat = conn.createStatement();
            ResultSet hasil = stat.executeQuery(sql);
            while(hasil.next()){
            String idpembayaran = hasil.getString("idpembayaran");
            String nama = hasil.getString("nama");
            String tanggalbayar = hasil.getString("tanggalbayar");
            String gunabayar = hasil.getString("gunabayar");
            String jumlahbayar = hasil.getString("jumlahbayar");

                     
            String[] data = {idpembayaran,nama,tanggalbayar,gunabayar,jumlahbayar};
            tabmode.addRow(data);
            jumdata = jumdata+1;
            }
        }catch (SQLException sqle){
            jumdata=0;
            JOptionPane.showMessageDialog(null,"Data Gagal Masuk Tabel"+sqle);
        }
        lbljum5.setText(jumdata+" Item");
        // TODO add your handling code here:
    }//GEN-LAST:event_tabelpembayaranAncestorAdded

    private void txtcaripembayaranKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtcaripembayaranKeyPressed
        // TODO add your handling code here:
        datatable_pembayaran();
    }//GEN-LAST:event_txtcaripembayaranKeyPressed

    private void txtcaripembayaranKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtcaripembayaranKeyTyped
        // TODO add your handling code here:
        datatable_pembayaran();
    }//GEN-LAST:event_txtcaripembayaranKeyTyped

    private void bcaripembayaranActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bcaripembayaranActionPerformed
        // TODO add your handling code here:
        datatable_pembayaran();
        kosong();
    }//GEN-LAST:event_bcaripembayaranActionPerformed

    private void txtidpelangganKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtidpelangganKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtidpelangganKeyPressed

    private void txtidpelangganKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtidpelangganKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_txtidpelangganKeyTyped

    private void jLabel20MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel20MouseClicked
    try{
        String path="./src/report/cetakpelanggan.jasper";
        HashMap parameter = new HashMap();
        parameter.put("idpelanggan", txtidpelanggan.getText());
        JasperPrint print = JasperFillManager.fillReport(path,parameter,conn);
        JasperViewer.viewReport(print,false);
    } catch (Exception ex){
        JOptionPane.showMessageDialog(rootPane,"Dokumen Tidak Ada"+ex);
    }    
    kosong();
        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel20MouseClicked

    private void jLabel28MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel28MouseClicked
     try{
        String path="./src/report/reportpelanggan.jasper";
        HashMap parameter = new HashMap();
        parameter.put("idpelanggan", txtidpelanggan.getText());
        JasperPrint print = JasperFillManager.fillReport(path,parameter,conn);
        JasperViewer.viewReport(print,false);
    } catch (Exception ex){
        JOptionPane.showMessageDialog(rootPane,"Dokumen Tidak Ada"+ex);
    }    
    kosong();
        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel28MouseClicked

    private void txtidpaketwisataKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtidpaketwisataKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtidpaketwisataKeyPressed

    private void txtidpaketwisataKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtidpaketwisataKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_txtidpaketwisataKeyTyped

    private void jLabel30MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel30MouseClicked
     try{
        String path="./src/report/cetakpaketwisata.jasper";
        HashMap parameter = new HashMap();
        parameter.put("idpaketwisata", txtidpaketwisata.getText());
        JasperPrint print = JasperFillManager.fillReport(path,parameter,conn);
        JasperViewer.viewReport(print,false);
    } catch (Exception ex){
        JOptionPane.showMessageDialog(rootPane,"Dokumen Tidak Ada"+ex);
    }    
    kosong();
        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel30MouseClicked

    private void jLabel31MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel31MouseClicked
     try{
        String path="./src/report/reportpaketwisata.jasper";
        HashMap parameter = new HashMap();
        parameter.put("idpaketwisata", txtidpaketwisata.getText());
        JasperPrint print = JasperFillManager.fillReport(path,parameter,conn);
        JasperViewer.viewReport(print,false);
    } catch (Exception ex){
        JOptionPane.showMessageDialog(rootPane,"Dokumen Tidak Ada"+ex);
    }   
    kosong();
        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel31MouseClicked

    private void txtidbusKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtidbusKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtidbusKeyPressed

    private void txtidbusKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtidbusKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_txtidbusKeyTyped

    private void jLabel33MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel33MouseClicked
    try{
        String path="./src/report/cetakarmada.jasper";
        HashMap parameter = new HashMap();
        parameter.put("idbuswisata", txtidbus.getText());
        JasperPrint print = JasperFillManager.fillReport(path,parameter,conn);
        JasperViewer.viewReport(print,false);
    } catch (Exception ex){
        JOptionPane.showMessageDialog(rootPane,"Dokumen Tidak Ada"+ex);
    }   
    kosong();
        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel33MouseClicked

    private void jLabel34MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel34MouseClicked
     try{
        String path="./src/report/reportarmada.jasper";
        HashMap parameter = new HashMap();
        parameter.put("idbuswisata", txtidbus.getText());
        JasperPrint print = JasperFillManager.fillReport(path,parameter,conn);
        JasperViewer.viewReport(print,false);
    } catch (Exception ex){
        JOptionPane.showMessageDialog(rootPane,"Dokumen Tidak Ada"+ex);
    }   
    kosong();
        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel34MouseClicked

    private void txtidpembayaranKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtidpembayaranKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtidpembayaranKeyPressed

    private void txtidpembayaranKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtidpembayaranKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_txtidpembayaranKeyTyped

    private void jLabel39MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel39MouseClicked
     try{
        String path="./src/report/cetakpembayaran.jasper";
        HashMap parameter = new HashMap();
        parameter.put("idpembayaran", txtidpembayaran.getText());
        JasperPrint print = JasperFillManager.fillReport(path,parameter,conn);
        JasperViewer.viewReport(print,false);
    } catch (Exception ex){
        JOptionPane.showMessageDialog(rootPane,"Dokumen Tidak Ada"+ex);
    }   
    kosong();
        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel39MouseClicked

    private void jLabel40MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel40MouseClicked
     try{
        String path="./src/report/reportpembayaran.jasper";
        HashMap parameter = new HashMap();
        parameter.put("idpembayaran", txtidpembayaran.getText());
        JasperPrint print = JasperFillManager.fillReport(path,parameter,conn);
        JasperViewer.viewReport(print,false);
    } catch (Exception ex){
        JOptionPane.showMessageDialog(rootPane,"Dokumen Tidak Ada"+ex);
    }   
    kosong();
        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel40MouseClicked

    private void jLabel41MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel41MouseClicked
     new homeuser().show();
    this.dispose();
        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel41MouseClicked

    private void jLabel42MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel42MouseClicked
        new login().show();
        this.dispose();
        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel42MouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
     
    
    
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(laporan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(laporan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(laporan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(laporan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new laporan().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bcaribus;
    private javax.swing.JButton bcaripaketwisata;
    private javax.swing.JButton bcaripelanggan;
    private javax.swing.JButton bcaripembayaran;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JLabel lbljum;
    private javax.swing.JLabel lbljum2;
    private javax.swing.JLabel lbljum3;
    private javax.swing.JLabel lbljum5;
    private javax.swing.JPanel panelbus;
    private javax.swing.JPanel panelpaketwisata;
    private javax.swing.JPanel panelpelanggan;
    private javax.swing.JPanel paneltravelreguler1;
    private javax.swing.JTable tabelbuswisata;
    private javax.swing.JTable tabelpaketwisata;
    private javax.swing.JTable tabelpelanggan;
    private javax.swing.JTable tabelpembayaran;
    private javax.swing.JTabbedPane tabpane;
    private javax.swing.JTextField txtcaribus;
    private javax.swing.JTextField txtcaripaketwisata;
    private javax.swing.JTextField txtcaripelanggan;
    private javax.swing.JTextField txtcaripembayaran;
    private javax.swing.JTextField txtidbus;
    private javax.swing.JTextField txtidpaketwisata;
    private javax.swing.JTextField txtidpelanggan;
    private javax.swing.JTextField txtidpembayaran;
    // End of variables declaration//GEN-END:variables
}
